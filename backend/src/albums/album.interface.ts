// src/albums/album.interface.ts

export interface BaseAlbum {
  title: string;
  artistId: number;
  description: string;
  image: string;
}

export interface Album extends BaseAlbum {
  id: number;
}

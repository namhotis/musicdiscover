// src/albums/album.interface.ts

import { Album } from "./album.interface";

export interface Albums {
  [key: number]: Album;
}
